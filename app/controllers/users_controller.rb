class UsersController < ApplicationController
  def index
    users = User.all
    render :json => users
  end
  
  def create
    user = User.new(user_params)
    # in Rails 4 you must whitelist attributes with #permit
    if user.save
      render :json => user
    else
      render :json => user.errors.full_messages,
             :status => :unprocessable_entity
    end
  end
  
  def show
    u = User.find(params[:id])
    render :json => u
  end
  
  def update
    p 'entering the update'
    u = User.find(params[:id])
    p params
    if u.update_attributes(user_params)
      render :json => u
    else
      render :json => u.errors
    end
  end
  
  def destroy
    u = User.find(params[:id])
    u.destroy
    render :json => u
  end
  
  private 
  
  def user_params
    params.require(:user).permit(:fname, :email)
  end
end
