# == Schema Information
#
# Table name: contacts
#
#  id         :integer          not null, primary key
#  name       :string(255)      not null
#  email      :string(255)      not null
#  user_id    :integer          not null
#  created_at :datetime
#  updated_at :datetime
#

class Contact < ActiveRecord::Base
  validates :email, presence: true, uniqueness: {:scope => :user_id}
  validates :name, :user_id, presence: true
  
  belongs_to(
    :user,
    foreign_key: :user_id,
    primary_key: :id,
    class_name: "User"
  )
  
  has_many :comments, :as => :commentable
  
  has_many(
    :contact_shares,
    foreign_key: :contact_id,
    primary_key: :id,
    class_name: "ContactShare"
  )
  
  def self.contacts_for_user_id(user_id)
    self.find_by_sql([<<-SQL, user_id, user_id])
    SELECT
      contacts.*
    FROM
      contacts
    LEFT OUTER JOIN
      contact_shares ON contact_shares.contact_id = contacts.id
    WHERE
      contact_shares.user_id = ?
    OR
      contacts.user_id = ?
    SQL
  end
  
end
