require 'addressable/uri'
require 'rest-client'


url = Addressable::URI.new(
  scheme: 'http',
  host: 'localhost',
  port: 3000,
  path: '/users/6',
).to_s

puts RestClient.patch(
  url,
  { :user => { :fname => "Gizmo2", :email => "gizmo@gizmo.gizmo"} }
)
