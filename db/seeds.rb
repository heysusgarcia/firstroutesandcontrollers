# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or create!d alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create!([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create!(name: 'Emanuel', city: cities.first)
ActiveRecord::Base.transaction do

  u1 = User.create!(fname: "mark", email: "mark@mark.com")
  u2 = User.create!(fname: "gizmo", email: "gizmo@gizmo.com")
  u3 = User.create!(fname: "markov", email: "markov@markov.com")

  c1 = Contact.create!(name: "mark", email: "mark@mark.com", user_id: u1.id )
  c2 = Contact.create!(name: "gizmo", email: "gizmo@gizmo.com", user_id: u2.id )
  c3 = Contact.create!(name: "markov", email: "markov@markov.com", user_id: u3.id )

  cs1 = ContactShare.create!(contact_id: c1.id, user_id: u2.id)
  cs2 = ContactShare.create!(contact_id: c2.id, user_id: u1.id)
  cs3 = ContactShare.create!(contact_id: c3.id, user_id: u1.id)

  com1 = Comment.create!(body: "hellllooo", author_id: u1.id, commentable_type: "User", commentable_id: u2.id)
  com2 = Comment.create!(body: "another comment", author_id: u3.id, commentable_type: "Contact", commentable_id: c1.id)
  com3 = Comment.create!(body: "here's another comment", author_id: u2.id, commentable_type: "Contact", commentable_id: u1.contacts.first.id)

end
