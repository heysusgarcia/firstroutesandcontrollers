class ChangeComments < ActiveRecord::Migration
  def change
    rename_column :comments, :comment_type, :commentable_type
  end
end
