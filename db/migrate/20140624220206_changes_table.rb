class ChangesTable < ActiveRecord::Migration
  def change
    change_column(:users, :fname, :string, null: false)
    change_column(:users, :email, :string, null: false)
    
    add_index(:users, :fname, unique: true)
    add_index(:users, :email, unique: true) 
  end
end
